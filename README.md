# OpenML dataset: sleuth_case2002

https://www.openml.org/d/665

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Contains 110 data sets from the book 'The Statistical Sleuth'
by Fred Ramsey and Dan Schafer; Duxbury Press, 1997.
(schafer@stat.orst.edu) [14/Oct/97] (172k)

Note: description taken from this web site:
http://lib.stat.cmu.edu/datasets/

File: ../data/sleuth/case2002.asc


Information about the dataset
CLASSTYPE: numeric
CLASSINDEX: none specific

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/665) of an [OpenML dataset](https://www.openml.org/d/665). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/665/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/665/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/665/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

